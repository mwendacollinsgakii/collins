const text = "Greetings! I'm Collins Mwenda, a software developer with a passion for crafting elegant solutions to complex \n"+
"problems. My journey into the world of programming began with a curious mind and a determination to \n"+
"learn. Through countless hours of self-study and hands-on projects, I've honed my skills in languages like \n"+
"html, css, Python, JavaScript, and Dart, and I'm constantly exploring new technologies to expand my toolkit. \n"+
"I thrive in dynamic environments where innovation and collaboration drive progress, and I'm excited to bring \n"+
"my unique perspective and skills to contribute meaningfully to the tech industry. Join me as I continue to \n"+
"evolve as a developer and create impactful software solutions. Thank you!";

let index = 0;
const span = document.getElementById('aboutp');

function typeWriter() {
    if (index < text.length) {
        span.textContent += text.charAt(index);
        index++;
        setTimeout(typeWriter, 40);
    }
}

typeWriter();
